<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'results';

    protected $fillable = ['id', 'tag_id', 'user_id', 'slug', 'lang', 'image', 'visible', 'ip'];

    public function tag(){
        return $this->belongsTo('App\Tag');
    }

    public function quizz(){
        return $this->belongsTo('App\Quizz');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
