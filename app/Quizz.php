<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quizz extends Model
{
    protected $table = 'quizzs';

    protected $fillable = ['id', 'slug', 'visible', 'image', 'gender', 'langs', 'playImage', 'type', 'created_at'];

    public function translations(){
        return $this->hasMany('App\Translation');
    }

    public function results(){
        return $this->hasMany('App\Result');
    }

    public function visits(){
        return $this->hasMany('App\Visit');
    }

    public function answers(){
        return $this->hasMany('App\Answer');
    }

    public function getTranslationAttribute(){
        if (!array_key_exists('translations', $this->relations))
            $this->load('translations');
        $related = $this->getRelation('translations')->where('lang', \App::getLocale())->first();

        if(!empty($related->data)) $related = json_decode($related->data); else return false;
        $translations = collect();
        foreach($related as $key => $value){
            $translations->$key = $value;
        }
        return ($translations) ? $translations : 'NaN';
    }
}
