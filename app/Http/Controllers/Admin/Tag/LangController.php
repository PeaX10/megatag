<?php

namespace App\Http\Controllers\Admin\Tag;

use App\Tag;
use App\Translation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class LangController extends Controller
{
    /**
     * @param Request $request
     * @param $quizz_id
     * @param $lang
     * @return array
     */
    public function destroy(Request $request, $tag_id, $lang){
        if($request->ajax()){
            $tag = Tag::findOrFail($tag_id);
            $tag->translations()->where('lang', $lang)->firstOrFail()->delete();
            $langs = explode(',', $tag->langs);
            foreach($langs as $key => $value){
                if($value == $lang || $value == '') unset($langs[$key]);
            }
            $tag->langs = implode(',', $langs);
            $tag->save();
            return ['success' => true];
        }else{
            return abort(404);
        }
    }

    public function show($tag_id, $trans_id){
        return redirect('translation/'.$trans_id);
    }

    public function edit($tag_id, $lang){
        $lang = Translation::where('lang', $lang)->where('tag_id', $tag_id)->firstOrFail();
        if($lang->visible){
            $lang->visible = 0;
        }else{
            $lang->visible = 1;
        }
        $lang->save();
        return back()->with('lang_added', $lang->lang);
    }

    public function store(Request $request, $tag_id){

        if($request->ajax() && !Tag::where('id', $tag_id)->where('langs', 'LIKE', '%'.Input::get('lang').'%')->exists()){
            $lang = new Translation();
            $lang->lang = Input::get('lang');
            $lang->tag_id = Input::get('tag_id');
            $data = array('title' => 'NaN');
            $lang->data = json_encode($data);
            $lang->save();

            $tag = Tag::findOrFail($tag_id);
            $langs = explode(',', $tag->langs);
            array_push($langs, Input::get('lang'));
            $tag->langs = implode(',', $langs);
            $tag->save();

            return json_encode($lang);
        }else{
            return abort(404);
        }
    }
}
