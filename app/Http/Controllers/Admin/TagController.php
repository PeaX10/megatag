<?php

namespace App\Http\Controllers\Admin;

use App\Translation;
use Illuminate\Http\Request;
use App\Tag;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class TagController extends Controller
{
    public function index(){
        $tags = Tag::orderBy('id', 'desc')->with('translations')->get();
        return view('back.tag.index', compact('tags'));
    }

    public function destroy(Request $request, $id){
        if($request->ajax()){
            $tag = Tag::findOrFail($id);
            unlink(public_path('uploads/tag/'.$tag->image));
            $tag->translations()->delete();
            $tag->visits()->delete();
            foreach($tag->results()->get() as $key => $value){
                if(file_exists(public_path('uploads/result/'.$value->image))) unlink(public_path('uploads/result/'.$value->image));
            }
            $tag->results()->delete();
            foreach($tag->answers()->get() as $key => $value){
                if(file_exists(public_path('uploads/answer/'.$value->background))) unlink(public_path('uploads/answer/'.$value->background));
                if(file_exists(public_path('uploads/answer/preview/'.$value->id.'.jpg'))) unlink(public_path('uploads/answer/preview/'.$value->id.'.jpg'));
                $builder = json_decode($value->builder);
                $composants = $builder->composants;
                foreach($composants as $key2 => $value2){
                    if(!empty($value2->image) && file_exists(public_path('uploads/composant/'.$value2->image))){
                        unlink(public_path('uploads/composant/'.$value2->image));
                    }
                    if(!empty($value2->custom_mask) && file_exists(public_path('uploads/composant/'.$value2->custom_mask))){
                        unlink(public_path('uploads/composant/'.$value2->custom_mask));
                    }
                }
            }
            $tag->answers()->delete();
            $tag->delete();
            return ['success' => true];
        }else{
            return abort(404);
        }
    }

    public function create(){
        return view('back.tag.create');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:5',
            'gender' => 'required|in:all,male,female',
            'image' => 'required|image|max:8192|dimensions:width=1200,height=630'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            // On encode notre slug et on regarde si il existe ou non en BDD
            $slug = str_slug(str_random(rand(1,5)).time().str_random(rand(2, 6)));
            $ext = Input::file('image')->getClientOriginalExtension();
            $image_name =  't_'.time().str_random(rand(15, 20)).'.'.$ext;
            if($ext != 'gif'){
                $image = Image::make(Input::file('image'))->save(public_path('uploads/tag/'.$image_name));
            }else{
                Input::file('image')->move(public_path('uploads/tag'), $image_name);
            }
            $tag = new Tag();
            $tag->slug = $slug;
            if(Input::get('visible')) $tag->visible = 1;
            $tag->image = $image_name;
            $tag->gender = Input::get('gender');
            $tag->langs = implode(',', Input::get('langs'));
            $tag->save();

            foreach(Input::get('langs') as $key => $value){
                $lang = new Translation();
                $lang->tag_id = $tag->id;
                $lang->lang = $value;
                $data = array('title' => Input::get('name'));
                $lang->data = json_encode($data);
                $lang->save();
            }

            return redirect('tag')->with('added_tag', true);
        }

    }

    public function edit($id){
        $tag = Tag::findOrFail($id);
        $langs = explode(',', $tag->langs);
        $visits = $tag->visits()->orderBy('created_at', 'desc')->get();
        $results = $tag->results()->orderBy('created_at', 'desc')->get();
        $answers = $tag->answers()->orderBy('created_at', 'desc')->get();
        return view('back.tag.edit', compact('tag', 'langs', 'visits', 'results', 'answers'));
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'slug' => 'required|unique:tags,slug,'.$id.'|min:5',
            'gender' => 'required|in:all,male,female',
            'image' => 'image|max:8192|dimensions:min_width=1200,min_height=630'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            // On encode notre slug et on regarde si il existe ou non en BDD
            $slug = str_slug(Input::get('slug'));
            if(Tag::where('slug', $slug)->whereNotIn('id', [$id])->exists()) {
                $validator->errors()->add('slug', 'Ce slug existe déjà !');
                return back()->withErrors($validator)->withInput();
            }else{
                $tag = Tag::findOrFail($id);
                if($request->hasFile('image')){
                    if(file_exists(public_path('uploads/tag/'.$tag->image))) unlink(public_path('uploads/tag/'.$tag->image));
                    $ext = Input::file('image')->getClientOriginalExtension();
                    $image_name =  't_'.time().str_random(rand(15, 20)).'.'.$ext;
                    if($ext != 'gif'){
                        $image = Image::make(Input::file('image'))->save(public_path('uploads/tag/'.$image_name));
                    }else{
                        Input::file('image')->move(public_path('uploads/tag'), $image_name);
                    }
                    $tag->image = $image_name;
                }
                $tag->slug = $slug;
                if(Input::get('visible')) $tag->visible = 1; else $tag->visible = 0;
                $tag->gender = Input::get('gender');
                $tag->save();

                return back()->with('edited_tag', true);
            }
        }
    }
}
