<?php

namespace App\Http\Controllers\Admin;

use App\Answer;
use App\Stats;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DesignerValidation;
use App\Quizz;
use Carbon\Carbon;

class PageController extends Controller
{
    public function index(){
        return view('back.dashboard');
    }

    public function design(){
        $designs = DesignerValidation::where('validated', 0)->get();
        return view('back.design', compact('designs'));
    }

    public function validDesign($id){
        $design = DesignerValidation::where('id', $id)->firstOrFail();
        $data = json_decode($design->data);
        if($design->type == 'answer'){
            $answer = Answer::find($design->design_id);
            $answer->background = $data->background;
            $answer->save();
            if(file_exists(public_path('uploads/answer/preview/p_'.$design->design_id.'.jpg'))) unlink(public_path('uploads/answer/preview/p_'.$design->design_id.'.jpg'));
        }else if($design->type == 'quizz'){
            $quizz = Quizz::find($design->design_id);
            $quizz->image = $data->image;
            $quizz->playImage = $data->playImage;
            $quizz->save();
        }

        $design->validated = 1;
        $design->save();
        return back()->with('validated', true);
    }

    public function deleteDesign($id){
        $design = DesignerValidation::where('id', $id)->firstOrFail();
        $data = json_decode($design->data);
        if($design->type == 'answer'){
            if(file_exists(public_path('uploads/answer/preview/p_'.$design->design_id.'.jpg'))) unlink(public_path('uploads/answer/preview/p_'.$design->design_id.'.jpg'));
            if(file_exists(public_path('uploads/answer/'.$data->background))) unlink(public_path('uploads/answer/'.$data->background));
        }else if($design->type == 'quizz'){
            if(file_exists(public_path('uploads/quizz/'.$data->image))) unlink(public_path('uploads/quizz/'.$data->image));
            if(file_exists(public_path('uploads/quizz/'.$data->playImage))) unlink(public_path('uploads/quizz/'.$data->playImage));
        }
        $design->delete();
        return back()->with('deleted', true);
    }

    public function stats(){
        // On récupère les 12 derniers mois et on affiche ça par mois
        $stats = Stats::whereDate('created_at', '=', Carbon::today()->toDateString()->subYear());
        return view('back.stats', compact('stats'));
    }

    public function statsQuizz($id_quizz){
        return view('back.statsquizz', compact('id_quizz'));
    }
}
