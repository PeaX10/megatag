<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stats extends Model
{
    protected $table = 'stats';

    protected $fillable = ['id', 'quizz', 'user', 'type', 'ip', 'created_at'];

    public function quizz(){
        return $this->belongsTo('App\Quizz');
    }

}
