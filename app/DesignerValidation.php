<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignerValidation extends Model
{
    protected $table = 'designer_validations';

    protected $fillable = ['id', 'user_id', 'type', 'data', 'validated'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
