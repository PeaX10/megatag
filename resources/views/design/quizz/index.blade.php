@extends('design.layouts.default', ['active' => 'quizz'])

@section('title') Quizzs @endsection

@section('content')
    <div class="col-md-12">
        <div class="card">

            <div class="toolbar">
                <a href="{{ url('quizz/create') }}" id="add_quizz" class="btn btn-danger btn-round btn-fill"><i class="fa fa-plus"></i></a>
            </div>

            <table id="bootstrap-table" class="table">
                <thead>
                <th data-field="id" data-sortable="true" class="text-center">ID</th>
                <th data-field="slug" class="text-center" data-sortable="true">Nom</th>
                <th data-field="image" data-sortable="false" class="text-center hidden-sm hidden-xs">Image</th>
                <th data-field="actions" class="text-right" data-events="operateEvents" data-formatter="operateFormatter">Actions</th>
                </thead>
                <tbody>
                @foreach($quizzs as $quizz)
                    <tr>
                        <td>{{ $quizz->id }}</td>
                        <td><b>
                            @if(!empty($quizz->translation->title))
                                {{ $quizz->translation->title }}
                            @else
                                {{ $quizz->slug }}
                            @endif
                            </b>
                        </td>
                        <td class="hidden-sm hidden-xs"><div align="center">@if(!empty($quizz->image))<img class="img-responsive img-rounded img-raised" style="max-width:100px" src="{{ url('uploads/quizz/'.$quizz->image) }}" />@endif</div></td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/bootstrap-checkbox-radio-switch-quizzs.js') }}"></script>
    <script type="text/javascript">
        var $table = $('#bootstrap-table');

        function operateFormatter(value, row, index) {
            return [
                '<a rel="tooltip" title="Voir" class="btn btn-simple btn-info btn-icon table-action view" href="javascript:void(0)">',
                '<i class="fa fa-image"></i>',
                '</a>',
                '<a rel="tooltip" title="Modifier" class="btn btn-simple btn-warning btn-icon table-action edit" href="./quizzes/'+row.id+'/edit">',
                '<i class="fa fa-edit"></i>',
                '</a>'
            ].join('');
        }

        $().ready(function(){
            window.operateEvents = {
                'click .view': function (e, value, row, index) {
                    content =   '<h6>'+row.slug+'<span class="label label-info">ID: '+row.id+'</span></h6>' +
                                '<br>' +
                                row.image+'<br>' +
                                '<span>En ligne ?: </span>' +
                                row.visible+'<br>'+
                                '<span>Langues: </span>' +
                                row.langs+ '<br>' +
                                '<span>Sexes: </span>' +
                                row.sexes+ '<br>' +
                                '<span>Date de création: </span>' + row.created_at + '<br>' +
                                '<span>Dernière édition: </span>' + row.updated_at;
                    swal({
                        title: 'Information du quizz',
                        html: content,
                        showCancelButton: false,
                        showConfirmButton: false,
                        allowOutsideClick: true
                    });

                    console.log(info);
                }
            };

            $table.bootstrapTable({
                toolbar: ".toolbar",
                toolbarAlign: 'right',
                clickToSelect: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 15,
                clickToSelect: false,
                pageList: [15,30,50,100,200],

                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " quizzs par page";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });

            //activate the tooltips after the data table is initialized
            $('[rel="tooltip"]').tooltip();

            $(window).resize(function () {
                $table.bootstrapTable('resetView');
            });

        });

    </script>
@endsection