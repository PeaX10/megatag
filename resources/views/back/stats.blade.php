@extends('back.layouts.default', ['active' => 'stats'])

@section('title') Statistiques @endsection

@section('content')
    <div class="col-md-12">
            <h2><br><small>Statistiques global du site</small></h2>
            <div class="card">
                <div id="chartPerformance" class="ct-chart ct-perfect-fourth"></div>
            </div>

            <h2></small> <br><small>NASDAQ: AAPL</small></h2>
            <div class="card"><div id="chartStock" class="ct-chart ct-perfect-fourth"></div></div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/bootstrap-checkbox-radio-switch-quizzs.js') }}"></script>
    <script type="text/javascript">
        var $table = $('#bootstrap-table');

        function operateFormatter(value, row, index) {
            return [
                '<a rel="tooltip" title="Approuver" class="btn btn-simple btn-success btn-icon table-action valid" href="./design/'+row.id+'/valid">',
                '<i class="fa fa-check"></i>',
                '</a>',
                '</a>',
                '<a rel="tooltip" title="Rejeter" class="btn btn-simple btn-danger btn-icon table-action valid" href="./design/'+row.id+'/delete">',
                '<i class="fa fa-times"></i>',
                '</a>'
            ].join('');
        }

        $().ready(function(){

            var dataPerformance = {
                labels: ['9pm', '2am', '8am', '2pm', '8pm'],
                series: [
                    [1, 6, 8, 7, 4, 7, 8, 12, 16, 17, 14, 13]
                ]
            };

            var optionsPerformance = {
                showPoint: false,
                lineSmooth: true,
                axisX: {
                    showGrid: false,
                    showLabel: true
                },
                axisY: {
                    offset: 40,
                },
                low: 0,
                high: 20
            };

            Chartist.Line('#chartPerformance', dataPerformance, optionsPerformance);

            var dataStock = {
                labels: ['2009', '2010', '2011', '2012', '2013', '2014'],
                series: [
                    [22.20, 28.16, 34.90, 42.28, 47.26, 48.89, 51.93, 55.32, 59.21, 62.21, 75.50, 80.23, 60.32, 55.03, 62.21, 78.83, 82.12, 89.21, 102.50, 107.23]
                ]
            };

            var optionsStock = {
                lineSmooth: false,
                axisY: {
                    offset: 40,
                    labelInterpolationFnc: function(value) {
                        return '$' + value;
                    }

                },
                low: 10,
                high: 110,
                classNames: {
                    point: 'ct-point ct-green',
                    line: 'ct-line ct-green'
                }
            };

            var $chart = $('#chartStock');

            Chartist.Line('#chartStock', dataStock, optionsStock);

        });
        @if(Session::has('deleted'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - ce design a bien été supprimé."

        },{
            type: 'success',
            timer: 4000
        });
        @endif
        @if(Session::has('deleted'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - ce design a bien été approuvé."

        },{
            type: 'success',
            timer: 4000
        });
        @endif

    </script>
@endsection