@extends('back.layouts.default', ['active' => 'translation'])

@section('title') Traduction #{{ $trans->id }} - Lang: {{ $trans->lang }} @endsection

@section('content')
    <div class="row">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <?php
                        if(!empty($_GET['originalTrad']) && $_GET['originalTrad'] == 'en' && !empty($tradEn)){
                            $tradlang = 'en';
                            $trad = $tradEn;
                        }else{
                            $tradlang = 'fr';
                            $trad = $tradFr;
                        }
                        $trad = json_decode($trad->data);
                        ?>
                        <h4>
                            Traductions <small><span class="label label-info">{{ count(4) }}</span> @if($tradlang == 'fr' && !empty($tradEn))
                                    <a class="pull-right" href="{{ Request::url().'?originalTrad=en' }}"><img src="{{ url('img/flags/en.png') }}"></a>
                                @endif
                                @if($tradlang == 'en' && !empty($tradFr))
                                    <a class="pull-right" href="{{ Request::url().'?originalTrad=fr' }}"><img src="{{ url('img/flags/fr.png') }}"></a>
                                @endif</small>
                        </h4>
                        <hr>
                        {!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->action(url('translation/'.$trans->id))->put() !!}
                        @foreach($translations as $key => $value)
                            @if(!empty($trad->$key))
                                <div class="form-group">
                                    <label class="col-sm-4 col-lg-2 control-label" for="tradFR">
                                        <img src="{{ url('img/flags/'.$tradlang.'.png') }}">
                                    </label>
                                    <div class="col-sm-8 col-lg-10">
                                        <div style="border:1px solid #CCC; background:#EEEEEE; width:100%; padding:10px 18px; vertical-align: middle; border-radius: 3px; line-height:20px">
                                            <p style="margin-bottom: 0px">{{ $trad->$key }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            {!! BootForm::text($key, $key)->value($value) !!}
                            <hr>
                        @endforeach
                        {!! BootForm::submit('Modifier')->addClass('btn-info')->addClass('btn-fill') !!}
                        {!! BootForm::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection