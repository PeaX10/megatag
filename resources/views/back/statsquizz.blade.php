@extends('back.layouts.default', ['active' => 'stats'])

@section('title') Statistiques du quizz #{{ $id_quizz }}@endsection

@section('content')
    <div class="col-md-12">
            <h2><br><small>Performence du quizz sur 24h</small></h2>
            <div class="card">
                <div id="chartPerformance" class="ct-chart ct-perfect-fourth"></div>
            </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/bootstrap-checkbox-radio-switch-quizzs.js') }}"></script>
    <script type="text/javascript">
        var $table = $('#bootstrap-table');

        function operateFormatter(value, row, index) {
            return [
                '<a rel="tooltip" title="Approuver" class="btn btn-simple btn-success btn-icon table-action valid" href="./design/'+row.id+'/valid">',
                '<i class="fa fa-check"></i>',
                '</a>',
                '</a>',
                '<a rel="tooltip" title="Rejeter" class="btn btn-simple btn-danger btn-icon table-action valid" href="./design/'+row.id+'/delete">',
                '<i class="fa fa-times"></i>',
                '</a>'
            ].join('');
        }

        $().ready(function(){

            var dataPerformance = {
                labels: ['9pm', '2am', '8am', '2pm', '8pm'],
                series: [
                    [1, 6, 8, 7, 4, 7, 8, 12, 16, 17, 14, 13]
                ]
            };

            var optionsPerformance = {
                showPoint: false,
                lineSmooth: true,
                axisX: {
                    showGrid: false,
                    showLabel: true
                },
                axisY: {
                    offset: 40,
                },
                low: 0,
                high: 20
            };

            Chartist.Line('#chartPerformance', dataPerformance, optionsPerformance);

        });
        @if(Session::has('deleted'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - ce design a bien été supprimé."

        },{
            type: 'success',
            timer: 4000
        });
        @endif
        @if(Session::has('deleted'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - ce design a bien été approuvé."

        },{
            type: 'success',
            timer: 4000
        });
        @endif

    </script>
@endsection