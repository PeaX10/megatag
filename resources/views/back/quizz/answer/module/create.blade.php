@extends('back.layouts.default', ['active' => 'quizz'])

@section('title')
    Ajout d'un module
@endsection

@section('css')
    <link href="{{ url('css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/admin.css') }}" rel="stylesheet">
    <style>
        .colorpicker-2x .colorpicker-saturation {
            width: 200px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-hue,
        .colorpicker-2x .colorpicker-alpha {
            width: 30px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-color,
        .colorpicker-2x .colorpicker-color div {
            height: 30px;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <h4>Ajout du module</h4>
        <div class="card">
            <div class="content">
                {!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [3, 9]])->action( url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id.'/module') )->enctype("multipart/form-data") !!}
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <hr>
                @endif
                <h4>Informations</h4>
                <hr>
                {!! BootForm::text('Nom', 'name')->placeholder('Nom du module') !!}
                <div class="form-group type">
                    <label class="col-sm-4 col-lg-3 control-label">Type</label>
                    <div class="col-sm-8 col-lg-9">
                        <div class="col-md-4">
                            <label class="radio">
                                <input type="radio" data-toggle="radio" name="type" value="multivar" @if(old('type') == 'multivar') checked @endif> <i class="fa fa-table"></i>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label class="radio">
                                <input type="radio" data-toggle="radio" name="type" value="percent" @if(old('type') == 'percent') checked @endif> <i class="fa fa-percent"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <h4>Paramètres</h4>
                <hr>
                <div class="content-type multivar hidden">
                    <div class="input_fields_wrap">
                        <center><button class="btn btn-fill btn-primary add_field_button">Ajouter un élèment</button></center>
                        <hr>
                    </div>
                </div>
                <div class="content-type percent hidden">
                    {!! BootForm::text('Nombre de partie', 'parts')->type('number')->min(1)->max(100)->placeholder('Nombre de décomposition du chiffre total')->value(old('parts') ? old('parts') : 2) !!}
                </div>
                <br>
                <hr>
                <a href="{{ url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id) }}" class="btn btn-rounded btn-fill pull-leftt">Retour</a>
                <input type="submit" class="btn btn-rounded btn-fill btn-info pull-right" value="Créer">
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            if($('.type .radio input:checked').val() == 'multivar'){
                $('.content-type.percent').addClass('hidden');
                $('.content-type.multivar').removeClass('hidden');
            }else if($('.type .radio input:checked').val() == 'percent'){
                $('.content-type.multivar').addClass('hidden');
                $('.content-type.percent').removeClass('hidden');
            }
            $(".type .radio input[type=radio]" ).on( "change", function(){
                if($('.type .radio input:checked').val() == 'multivar'){
                    $('.content-type.percent').addClass('hidden');
                    $('.content-type.multivar').removeClass('hidden');
                }else if($('.type .radio input:checked').val() == 'percent'){
                    $('.content-type.multivar').addClass('hidden');
                    $('.content-type.percent').removeClass('hidden');
                }
            });
            $('#cp1').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
            $('#cp2').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
            $('#cp3').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
        });

        function addVar(variable){
            input = $('input[name="object"]');
            input.val(input.val()+variable);
        }

        $(document).ready(function() {
            var max_fields      = 100; //maximum input boxes allowed
            var wrapper         = $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed

                    $("#rm").remove();

                    $(wrapper).append('<div id="divs' + x + '">' +
                            '<div class="form-group">' +
                            '<label class="col-sm-4 col-lg-3 control-label" for="part">Nom de l\'élèment</label>' +
                            '<div class="col-sm-8 col-lg-9">' +
                            '<input type="text" name="elements_'+ x +'" id="elements" class="form-control" placeholder="Nom de l\'élèment">'+
                            '</div>'+
                            '<div class="form-group type">'+
                            '<label class="col-sm-4 col-lg-3 control-label">Type</label>'+
                            '<div class="col-sm-8 col-lg-9">'+
                            '<div class="col-md-4">'+
                            '<label class="radio checked">'+
                            '<span class="icons"><span class="first-icon fa fa-circle-o"></span><span class="second-icon fa fa-dot-circle-o"></span></span><input type="radio" data-toggle="radio" name="types_'+ x +'" value="picture"> <i class="fa fa-picture-o"></i>'+
                            '</label>'+
                            '</div>'+
                            '<div class="col-md-4">'+
                            '<label class="radio">'+
                            '<span class="icons"><span class="first-icon fa fa-circle-o"></span><span class="second-icon fa fa-dot-circle-o"></span></span><input type="radio" data-toggle="radio" name="types_'+ x +'" value="text"> <i class="fa fa-font"></i>'+
                            '</label> ' +
                            '</div> ' +
                            '</div><hr>' +
                            '</div></div>'
                    );
                    $(wrapper).append('<center><a href="#" id="rm" class="btn btn-fill btn-danger remove_field">Supprimer</a></center></div>'); //add input box
                    x++;
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault();
                x--;
                $("#divs"+ x).remove();

            })
        });
    </script>
@endsection