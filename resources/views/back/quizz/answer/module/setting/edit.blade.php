@extends('back.layouts.default', ['active' => 'quizz'])

@section('title')
    Modification des données
@endsection

@section('css')
    <link href="{{ url('css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/admin.css') }}" rel="stylesheet">
    <style>
        .colorpicker-2x .colorpicker-saturation {
            width: 200px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-hue,
        .colorpicker-2x .colorpicker-alpha {
            width: 30px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-color,
        .colorpicker-2x .colorpicker-color div {
            height: 30px;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <h4>Modification des données</h4>
        <div class="card">
            <div class="content">
                {!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [3, 9]])->action( url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id.'/module/'.$module_id.'/setting/'.$data_key.'/edit') )->put()->enctype("multipart/form-data") !!}
                {!! BootForm::bind($data) !!}
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <hr>
                @endif
                <h4>Données</h4>
                <hr>
                @foreach($schema as $value)
                    <?php $value_name = $value->name; ?>
                    @if($value->type == 'picture')
                        <div class="form-group">
                            <label class="col-sm-4 col-lg-3 control-label" for="visible">{{ $value->name }}</label>
                            <div class="col-sm-8 col-lg-9">
                                <label><small>Transférer une image</small></label>
                                <input 	type='file'
                                          class='input-ghost'
                                          name='image_{{ $value->name }}'
                                          style='visibility:hidden; height:0'
                                          onchange="$(this).next().find('input').val(($(this).val()).split('\\').pop());">
                                <div class="input-group input-file" name="Fichier_1">
                                        <span class="input-group-btn">
                                            <button 	class="btn btn-info btn-info btn-fill btn-choose"
                                                       type="button"
                                                       onclick="$(this).parents('.input-file').prev().click();">Choisir</button>
                                        </span>
                                    <input 	type="text"
                                              class="form-control"
                                              placeholder='Choisissez une image...'
                                              style="cursor:pointer"
                                              onclick="$(this).parents('.input-file').prev().click(); return false;"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 col-lg-3 control-label" for="visible">Aperçu</label>
                            <div class="col-sm-8 col-lg-9">
                                <img src="{{ url('uploads/module/'.$data->$value_name) }}" class="img-responsive img-rounded img-raised">
                            </div>
                        </div>
                    @else
                        <?php if(empty($data->$value_name)) $data->$value_name = ''; ?>
                        {!! BootForm::text($value->name, 'text_'.$value->name)->type('text')->placeholder('Entrer du texte')->value(old('text_'.$value->name) ? old('text_'.$value->name) : $data->$value_name) !!}
                    @endif
                @endforeach
                <div class="form-group">
                    <label class="col-md-3 control-label">Sexe</label>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <label class="radio">
                                <input type="radio" @if(empty(old('params_gender')) || old('params_gender') == 'all' || $data->params_gender == 'all') checked @endif data-toggle="radio" name="params_gender" value="all"> <i class="fa fa-venus-mars"></i>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label class="radio">
                                <input type="radio" @if(old('params_gender') == 'male' || $data->params_gender == 'male') checked @endif data-toggle="radio" name="params_gender" value="male"> <i class="fa fa-mars"></i>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label class="radio">
                                <input type="radio" @if(old('params_gender') == 'female' || $data->params_gender == 'female') checked @endif data-toggle="radio" name="params_gender" value="female"> <i class="fa fa-venus"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <hr>
                <a href="{{ url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id.'/module/'.$module_id.'/setting') }}" class="btn btn-rounded btn-fill pull-leftt">Retour</a>
                <input type="submit" class="btn btn-rounded btn-fill btn-info pull-right" value="Modifier">
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            if($('.mask .radio input:checked').val() == 'custom'){
                $('.custom-mask').removeClass('hidden');
            }else{
                $('.custom-mask').addClass('hidden');
            }
            $(".mask .radio input[type=radio]" ).on( "change", function(){
                if($('.mask .radio input:checked').val() == 'custom'){
                    $('.custom-mask').removeClass('hidden');
                }else{
                    $('.custom-mask').addClass('hidden');
                }
            });
            $('#cp1').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
            $('#cp2').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
            $('#cp3').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
        });

        function addVar(variable){
            input = $('input[name="object"]');
            input.val(input.val()+variable);
        }
    </script>
@endsection