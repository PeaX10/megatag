@extends('front.layouts.default')

@section('title')
    MEGATAG - @lang('app.home_title')
@endsection

@section('content')
    @if(count($tags))
    <div class="main">
        <div class="section text-center landing-section section-nude-gray" style="padding:0px">
            <div class="container">
                <h3 style="text-align:left; font-weight: bold; color: #333; font-size: 36px; font-family: montserrat_black !important; margin: 15px 0 -15px 0px;">@lang('app.latest_tags')</h3>
                <hr>
                <div class="row items-row quizzs">
                    @include('front.pages.quizzs')
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 text-center">
                        <button id="btn_load_more" class="btn btn-primary" style="width: 80%;padding: 15px;font-size: 23px;font-weight: bold;margin-top: 10px;color: #fff;background-color: #E73655;border-color: #b51f25;border-radius: 5px;"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>&nbsp;&nbsp;
                            @lang('app.discover_more')
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
          <div class="section section-white-gray text-center">
              <h2>@lang('app.sorry')</h2>
              <h5><p>@lang('app.sorry_no_quizz')</p></h5>
          </div>
    @endif
@endsection

@section('js')
    <script src="{{ url('js/jquery.infinitescroll.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(window).scroll(function() {
                if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                    $('#btn_load_more').trigger("click");
                }
            });
        });
    </script>
@endsection