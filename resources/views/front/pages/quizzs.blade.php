@foreach($tags as $tag)
    <div class="col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 20px; cursor: pointer" onclick="show_loader('{{ $tag->slug }}')">
        <div style="box-shadow: 0 0 10px #ccc;" id="quiz_container_{{ $tag->id }}" class="thumb_container">
            <div style="text-align: center; background: url('{{ url('uploads/quizz/'.$tag->playImage) }}'); background-size: cover; background-position: center center; background-repeat: no-repeat; height:30vh">
                <p class="label label-danger" style="font-size: 12px; float: right; color: #FFF !important; margin-top:25vh; margin-right:10px; background-color: #E73655"><i class="fa fa-play"></i> <b>Tag</b></p>
            </div>
            <div style="padding: 5px 10px; text-align: right">
                <p style="height:12vh; color:#333; padding-top:0vh; font-family: montserrat_black !important; font-weight: bold !important; text-align:left; font-size:20px"><b>{{ $tag->translation->title }}</b></p>
            </div>
        </div>
    </div>
@endforeach