<nav class="navbar navbar navbar-static-top" style="padding:0px" role="navigation-demo" id="navbar">
    <div class="container">

        <div class="navbar-header">
            <a href="{{ url('/') }}">
                <div class="logo-container" style="margin-left:5px">
                    <div class="logo">
                        <div class="hidden-xs hidden-sm"><img src="{{ url('img/logo.png') }}" alt="Logo MEGATAG"></div>
                        <div class="hidden-lg hidden-md"><img src="{{ url('img/logo_sm.png') }}" style="position:absolute; top:0; left:0; max-width: 61px" alt="Logo MEGATAG Mobile"></div>
                    </div>
                </div>
            </a>
        </div>
        <ul class="nav navbar-nav navbar-right" style="margin-right:5px; margin-top:10px">
            <li style="line-height:31px; padding-right: 20px">
                <div class="fb-like" data-href="@lang('app.facebook_link')" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false" data-width="80" style="width: 100%"></div>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="border: 2px solid #66615B; padding:8px 12px">
                    <span class="hidden-sm hidden-xs">{{ config('settings.langs')[App::getLocale()] }}</span>
                    <span class="hidden-lg hidden-md">{{ strtoupper(@App::getLocale()) }}</span>
                    <b class="caret"></b>
                    <div class="ripple-container"></div></a>
                <ul class="dropdown-menu" style="background: #FFF; border: 2px solid #66615B; min-width:50px">
                    @foreach(config('settings.langs') as $key => $lang)
                        <li><a href="https://{{ $key }}.megatag.net"><span class="hidden-md hidden-lg">{{strtoupper($key)}}</span><span class="hidden-xs hidden-sm">{{ $lang }}</span></a></li>
                    @endforeach
                </ul>
            </li>
        </ul>
    </div>
</nav>
