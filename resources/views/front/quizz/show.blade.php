@extends('front.layouts.default')

@section('title') {{ $pagequizz->translation->title }} - MEGATAG @endsection

@section('meta')
    <meta property="fb:app_id" content="1171151359613962">
    <meta property="og:site_name" content="Quizzeo.co">
    <meta property="og:type" content="website">
    @if(App::getLocale() == 'fr')
        <meta property="og:title" content="{{ preg_replace('/Tag/', 'Clique pour taguer', $pagequizz->translation->title, 1) }}" />
    @else
    <meta property="og:title" content="{{ $pagequizz->translation->title }}" />
    @endif
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:image" content="{{ url('uploads/quizz/'.$pagequizz->playImage) }}" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="420">
    <meta property="og:locale" content="{{ App::getLocale() }}">
    <meta name="twitter:card" content="photo">
    <meta name="twitter:title" content="{{ $pagequizz->translation->title }}">
    <meta name="twitter:image" content="{{ url('uploads/quizz/'.$pagequizz->playImage) }}">
    <meta name="author" content="MEGATAG">
@endsection

@section('content')
    <div class="section section-white-gray">
        <div class="container" style="padding:0px">
            <div class="col-md-8 col-sm-12" style="margin-bottom: 5px; padding:0px">
                <div id="result" style="padding: 5px; background: #fff; box-shadow: 0 0 4px #eee">
                    <div style="background: #FFEB78; border-radius: 5px;">

                        <div style="text-align: center; padding:0px; padding-top:20px">
                            <div class="hidden-xs hidden-sm">
                                <h2 class="text-center" style="margin: auto; padding-top:5px; line-height:33px; width: 80%;">
                                    <b style="font-size:60%; color:#333; font-family: montserrat_black">{{ $pagequizz->translation->title }}</b>
                                </h2>
                                <img src="{{ url('uploads/quizz/'.$pagequizz->playImage) }}" style="width:80%; padding-top:20px;" />
                            </div>

                            <div class="hidden-lg hidden-md">
                                <h2 class="text-center" style="margin:0; padding-top:5px; line-height:33px">
                                    <b style="font-size:60%; color:#333; font-family: montserrat_black">{{ $pagequizz->translation->title }}</b>
                                </h2>
                                <img src="{{ url('uploads/quizz/'.$pagequizz->playImage) }}" style="width:100%; padding-top:20px;" />
                            </div>
                        </div>
                        <div align="center">
                        @if(Auth::check())
                                <a href="{{ url('q/'.$pagequizz->slug.'/f') }}" class="btn btn-primary btn-fb" style="margin: 20px 0; font-weight: 700; background: #3b5998; box-shadow: 0 4px 4px -2px rgba(0,0,0,.25); border-radius: 6px; color:#FFF">
                                    <i class="fa fa-facebook-official" style="font-size: 35px; vertical-align: middle; margin-right: 15px;"></i>&nbsp;&nbsp;
                                    @lang('app.choose_friend') <img src="{{ url('uploads/avatar/'.Auth::user()->avatar) }}" style="max-width:40px; border-radius: 50%;" />
                                </a>
                        @else
                            <a href="{{ url('auth/facebook') }}" class="btn btn-primary btn-fb" style="margin: 20px 0; font-weight: 700; background: #3b5998; box-shadow: 0 4px 4px -2px rgba(0,0,0,.25); border-radius: 6px; color:#FFF">
                                <i class="fa fa-facebook-official" style="font-size: 35px; vertical-align: middle; margin-right: 15px;"></i>&nbsp;&nbsp;
                                @lang('app.choose_friend') <i class="fa fa-arrow-right"></i>
                            </a>
                        @endif
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-4 items-row hidden-xs hidden-sm">
                @foreach($quizzs->random(2) as $quizz)
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <center>
                            <a class="text-center" id="quiz_link" href="{{ url('q/'.$quizz->slug) }}" onclick="show_loader('{{ $quizz->slug }}')">
                                <div class="text-center" style="padding: 5px; margin-bottom:0px; border-radius: 5px; border:0px solid #fff; width: 100%; height: 180px; background-image: url('{{ url('uploads/quizz/'.$quizz->playImage) }}'); background-size: cover; background-position: center center">
                                </div>
                                <p style="text-align: left">
                                    <span style="color:#000; font-size: 16px">{{ $quizz->translation->title }}</span>
                                </p>
                            </a>
                        </center>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="section section-nude-gray" style="padding:0px">
        <div class="container" style="padding:0px">
            <div class="col-md-12" style="border-bottom: 1px solid #eee; margin-bottom: 10px">
                <p style="color:#333;font-size:36px; font-family: montserrat_black !important;margin: 10px 0 0 0px">
                    <b style="font-weight: 700">@lang('app.more_tags')</b>
                </p>
            </div>
            <div class="quizzs">
                @include('front.pages.quizzs', ['tags' => $quizzs])
            </div>
        </div>
        <div class="container" style="padding:0px">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 text-center">
                <button id="btn_load_more" class="btn btn-primary" style="width: 80%;padding: 15px;font-size: 23px;font-weight: bold;margin-top: 10px;color: #fff;background-color: #E73655;border-color: #b51f25;border-radius: 5px;"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>&nbsp;&nbsp;
                    @lang('app.discover_more')
                </button>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/bootstrap3-typeahead.min.js') }}" type="text/javascript"></script>
@endsection