@extends('front.layouts.default')

@section('title') {{ $pagequizz->translation->title }} - MEGATAG @endsection

@section('meta')
    <meta property="fb:app_id" content="1171151359613962">
    <meta property="og:site_name" content="Quizzeo.co">
    <meta property="og:type" content="website">
    @if(App::getLocale() == 'fr')
        <meta property="og:title" content="{{ preg_replace('/Tag/', 'Clique pour taguer', $pagequizz->translation->title, 1) }}" />
    @else
    <meta property="og:title" content="{{ $pagequizz->translation->title }}" />
    @endif
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:image" content="{{ url('uploads/quizz/'.$pagequizz->playImage) }}" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="420">
    <meta property="og:locale" content="{{ App::getLocale() }}">
    <meta name="twitter:card" content="photo">
    <meta name="twitter:title" content="{{ $pagequizz->translation->title }}">
    <meta name="twitter:image" content="{{ url('uploads/quizz/'.$pagequizz->playImage) }}">
    <meta name="author" content="MEGATAG">
@endsection

@section('content')
    <div class="section section-white-gray">
        <div class="container" style="padding:0px">
            <div class="col-md-8 col-sm-12" style="margin-bottom: 5px; padding:0px">
                <div id="result" style="padding: 5px; background: #fff; box-shadow: 0 0 4px #eee">
                    <div style="background: #FFEB78; border-radius: 5px;">
                        <div align="center" class="row" style="padding:20px;">
                                <h4 class="modal-title" id="friendModalLabel" style="font-family: montserrat !important; color:#000; padding-top:20px">@lang('app.choose_friend')</h4>
                                <br>
                                <div style="max-width: 500px">
                                    <input id="filter" name="filter" size="40" class="form-control" placeholder="Search friends" style="margin-bottom:20px; background: #FFF"/>
                                </div>
                                <div class="">
                                     <div id="search-div">
                                        @foreach($friends as $friend)
                                            <div class="col-sm-4" style="background: #FFF; border-radius: 3px; height:70px; border: 3px solid #FFEB78; cursor:pointer">
                                                <form action="{{ url('dq/'.$pagequizz->slug) }}" method="POST" style="padding:10px">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input name="friend" type="hidden" value="{{ $friend->name }}|{{ $friend->picture }}">
                                                    <img src='{{ $friend->picture }}' class='img-circle img-select' style='margin-right:10px; max-width:40px' /> {{ $friend->name }}
                                                </form>
                                            </div>
                                        @endforeach
                                     </div>
                                </div>
                                <div class="col-md-12"><input type="hidden" name="data" id="data_friend"></div>
                                <div class="col-md-12"><input type="submit" class="btn btn-danger" value="@lang('app.confirm')" style="margin-top:10px"></div>
                                <br>
                                <a id="syncFriend" style="padding-top:20px; color:#777; display: inline-block" href="{{ url('sync/friends') }}">@lang('app.sync_facebook_friends')</a>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-4 items-row hidden-xs hidden-sm">
                @foreach($quizzs->random(2) as $quizz)
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <center>
                            <a class="text-center" id="quiz_link" href="{{ url('q/'.$quizz->slug) }}" onclick="show_loader('{{ $quizz->slug }}')">
                                <div class="text-center" style="padding: 5px; margin-bottom:0px; border-radius: 5px; border:0px solid #fff; width: 100%; height: 180px; background-image: url('{{ url('uploads/quizz/'.$quizz->playImage) }}'); background-size: cover; background-position: center center">
                                </div>
                                <p style="text-align: left">
                                    <span style="color:#000; font-size: 16px">{{ $quizz->translation->title }}</span>
                                </p>
                            </a>
                        </center>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="section section-nude-gray" style="padding:0px">
        <div class="container" style="padding:0px">
            <div class="col-md-12" style="border-bottom: 1px solid #eee; margin-bottom: 10px">
                <p style="color:#333;font-size:36px; font-family: montserrat_black !important;margin: 10px 0 0 0px">
                    <b style="font-weight: 700">@lang('app.more_tags')</b>
                </p>
            </div>
            <div class="quizzs">
                @include('front.pages.quizzs', ['tags' => $quizzs])
            </div>
        </div>
        <div class="container" style="padding:0px">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 text-center">
                <button id="btn_load_more" class="btn btn-primary" style="width: 80%;padding: 15px;font-size: 23px;font-weight: bold;margin-top: 10px;color: #fff;background-color: #E73655;border-color: #b51f25;border-radius: 5px;"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>&nbsp;&nbsp;
                    @lang('app.discover_more')
                </button>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/bootstrap3-typeahead.min.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            $('#search-div .col-sm-4').click(function(){
                $(this).find('form').submit();
                $('.load-result.tag').show();
                setInterval(function(){
                    nb += 1;
                    if(nb <= 3){
                        $("span#loading").append(".");
                    }else{
                        nb = 1;
                        $("span#loading").html(".");
                    }
                }, 250);
            });

            $("#filter").keyup(function(){

                // Retrieve the input field text and reset the count to zero
                var filter = $(this).val(), count = 0;

                // Loop through the comment list
                $("#search-div .col-sm-4").each(function(){

                    // If the list item does not contain the text phrase fade it out
                    if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                        $(this).fadeOut();

                        // Show the list item if the phrase matches and increase the count by 1
                    } else {
                        $(this).show();
                        count++;
                    }
                });

                // Update the count
                var numberItems = count;
                $("#filter-count").text("Number of Comments = "+count);
            });
        });
    </script>
@endsection